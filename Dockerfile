# Imagem de Base httpd:2.4
# ----------------------------------------------
FROM httpd:2.4.46
# ----------------------------------------------
# Primeira Camada (Layer) copia o arquivo com o Form HTML para o diretorio 
# DocumentRoot mapeado no apache, neste caso o /usr/local/apache2/htdocs
# renomenado no destino para index.html.
# ----------------------------------------------
COPY desafio5/index.html /usr/local/apache2/htdocs/index.html
COPY desafio5/soma /usr/local/apache2/htdocs/soma
COPY desafio5/subtracao /usr/local/apache2/htdocs/subtracao
COPY desafio5/divisao /usr/local/apache2/htdocs/divisao
COPY desafio5/multiplicacao /usr/local/apache2/htdocs/multiplicacao
COPY entrypoint.sh /entrypoint.sh
# ----------------------------------------------
# Segunda Camada (Layer) copia o arquivo shell script (soma.cgi) para o 
# diretorio padrão da imagem para CGI (/usr/local/apache2/cgi-bin).
# ----------------------------------------------
#COPY soma.cgi /usr/local/apache2/cgi-bin/soma.cgi
# ----------------------------------------------
# Link de Referencia para habilitar o módulo de CGI no Apache2 (HTTPD)
# https://stackoverflow.com/questions/64743879/docker-httpd-apache-and-getting-cgi-bin-to-execute-perl-script
# ----------------------------------------------
RUN cp -rpv /usr/local/apache2/htdocs/subtracao/subtracao.cgi /usr/local/apache2/cgi-bin/ 
RUN cp -rpv /usr/local/apache2/htdocs/soma/soma.cgi /usr/local/apache2/cgi-bin/ 
RUN cp -rpv /usr/local/apache2/htdocs/multiplicacao/multiplicacao.cgi /usr/local/apache2/cgi-bin/ 
RUN cp -rpv /usr/local/apache2/htdocs/divisao/divisao.cgi /usr/local/apache2/cgi-bin/ 
RUN chmod +x /usr/local/apache2/cgi-bin/*.cgi && chmod +x /entrypoint.sh 
ENTRYPOINT ["/entrypoint.sh"]
#CMD httpd-foreground -c "LoadModule cgid_module modules/mod_cgid.so"
#CMD ["httpd-foreground -c \"LoadModule cgid_module modules/mod_cgid.so\""]
